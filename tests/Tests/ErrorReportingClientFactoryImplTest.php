<?php

declare(strict_types=1);

namespace DKXTests\NetteGCloudErrorReporting\Tests;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use DKX\NetteGCloudErrorReporting\ErrorReportingClientFactoryImpl;
use DKXTests\NetteGCloudErrorReporting\TestCase;
use Mockery;
use function assert;

final class ErrorReportingClientFactoryImplTest extends TestCase
{
	public function testCreate() : void
	{
		$credentials = [
			'type' => 'service_account',
			'client_email' => 'abcd@efgh.com',
			'private_key' => '',
		];

		$credentialsProvider = Mockery::mock(CredentialsProvider::class)
			->shouldReceive('getCredentials')->andReturn($credentials)->getMock();
		assert($credentialsProvider instanceof CredentialsProvider);

		$factory = new ErrorReportingClientFactoryImpl($credentialsProvider);
		$factory->create();
	}
}
