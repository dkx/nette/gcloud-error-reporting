# DKX/NetteGCloudErrorReporting

Google cloud error reporting integration for Nette DI

## Installation

```bash
$ composer require dkx/nette-gcloud
$ composer require dkx/nette-gcloud-error-reporting
```

## Usage

```yaml
extensions:
    gcloud: DKX\NetteGCloud\DI\GCloudExtension
    gcloud.errorReporting: DKX\NetteGCloudErrorReporting\DI\GCloudErrorReportingExtension
```

Now you'll be able to simply inject the `Google\Cloud\ErrorReporting\V1beta1\ReportErrorsServiceClient`.
