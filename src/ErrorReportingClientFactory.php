<?php

declare(strict_types=1);

namespace DKX\NetteGCloudErrorReporting;

use Google\Cloud\ErrorReporting\V1beta1\ReportErrorsServiceClient;

interface ErrorReportingClientFactory
{
	public function create() : ReportErrorsServiceClient;
}
