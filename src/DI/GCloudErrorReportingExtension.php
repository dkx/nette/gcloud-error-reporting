<?php

declare(strict_types=1);

namespace DKX\NetteGCloudErrorReporting\DI;

use DKX\NetteGCloudErrorReporting\ErrorReportingClientFactory;
use DKX\NetteGCloudErrorReporting\ErrorReportingClientFactoryImpl;
use Google\Cloud\ErrorReporting\V1beta1\ReportErrorsServiceClient;
use Nette\DI\CompilerExtension;
use Nette\DI\Definitions\Statement;
use Nette\Schema\Expect;
use Nette\Schema\Schema;
use function assert;
use function is_object;
use function is_string;

/**
 * @codeCoverageIgnore
 */
final class GCloudErrorReportingExtension extends CompilerExtension
{
	public function getConfigSchema() : Schema
	{
		return Expect::structure([
			'factory' => Expect::anyOf(Expect::string(), Expect::type(Statement::class))
				->default(ErrorReportingClientFactoryImpl::class),
		]);
	}

	public function loadConfiguration() : void
	{
		$builder = $this->getContainerBuilder();
		$config  = $this->getConfig();
		assert(is_object($config));

		$factory = $builder
			->addDefinition($this->prefix('factory'))
			->setType(ErrorReportingClientFactory::class)
			->setAutowired(false);

		if (is_string($config->factory)) {
			$factory->setFactory($config->factory);
		} elseif ($config->factory instanceof Statement) {
			assert(is_string($config->factory->entity));
			$factory->setFactory($config->factory->entity, $config->factory->arguments);
		}

		$builder
			->addDefinition($this->prefix('client'))
			->setType(ReportErrorsServiceClient::class)
			->setFactory([$factory, 'create']);
	}
}
