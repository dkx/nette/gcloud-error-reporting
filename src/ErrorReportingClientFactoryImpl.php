<?php

declare(strict_types=1);

namespace DKX\NetteGCloudErrorReporting;

use DKX\NetteGCloud\Credentials\CredentialsProvider;
use Google\Cloud\ErrorReporting\V1beta1\ReportErrorsServiceClient;

final class ErrorReportingClientFactoryImpl implements ErrorReportingClientFactory
{
	private CredentialsProvider $credentialsProvider;

	public function __construct(CredentialsProvider $credentialsProvider)
	{
		$this->credentialsProvider = $credentialsProvider;
	}

	/**
	 * @codeCoverageIgnore
	 */
	public function create() : ReportErrorsServiceClient
	{
		return new ReportErrorsServiceClient([
			'credentials' => $this->credentialsProvider->getCredentials(),
		]);
	}
}
